#![allow(unused)]

mod actions;
mod cli_args;
mod error;
mod utils;

use actions::{examine::examine_csvs, slurp::do_slurp};
use clap::Parser;
use cli_args::{Args, Commands};
use error::ToolError;

fn main() -> Result<(), ToolError> {
    let args = Args::parse();

    match args.command {
        Commands::ExamineCsvs { path } => {
            examine_csvs(path)?;
        }
        Commands::Slurp { path } => {
            do_slurp(path)?;
        }
    }

    Ok(())
}
