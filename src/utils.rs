use crate::error::ToolError;
use image_then_common::model::Document;
use std::{collections::HashMap, path::PathBuf};
use walkdir::{DirEntry, WalkDir};

type Record = HashMap<String, String>;

fn is_csv(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|e| e.to_lowercase().ends_with(".csv"))
        .unwrap_or(false)
}

pub fn ensure_trailing_slash(input: String) -> String {
    if input.ends_with('/') {
        input
    } else {
        format!("{input}/")
    }
}

pub fn walk_path_for_csvs(path: &PathBuf) -> Result<Vec<String>, ToolError> {
    let mut csvs = vec![];
    for entry in WalkDir::new(path) {
        let entry = entry.map_err(|error| ToolError::PathError(error.to_string()))?;
        if is_csv(&entry) {
            csvs.push(entry.path().display().to_string());

            // break;
        }
    }

    Ok(csvs)
}

pub fn map_csv(path: &PathBuf) -> Result<Document, ToolError> {
    let path_str = path.display().to_string();
    let mut record: Option<Record> = None;
    let mut contents =
        csv::Reader::from_path(path).map_err(|error| ToolError::ReadError(error.to_string()))?;

    let mut content_iterator: _ = contents.deserialize();
    if let Some(r) = content_iterator.next() {
        if record.is_none() {
            let r: Record =
                r.map_err(|error| ToolError::DeserializationError(error.to_string()))?;

            record = Some(r);
        }
        if content_iterator.next().is_some() {
            return Err(ToolError::TooManyRowsError(format!(
                "Too many rows in: {path_str}"
            )));
        }
    }

    match record {
        Some(e) => {
            let document = e.clone();

            Ok(match e.get("DrawerName") {
                Some(name) => match name.as_str() {
                    "Accounting" => Document::AccountingDocument(document),
                    "AP" => Document::AccountsPayableDocument(document.into()),
                    "Customer Service" => Document::MemberServiceDocument(document.into()),
                    "IT/IS" => Document::ItDocument(document),
                    _ => Document::MiscDocument(document),
                },
                _ => Document::MiscDocument(document),
            })
        }
        _ => Err(ToolError::NotEnoughRowsError(format!(
            "Not enough rows in: {path_str}"
        ))),
    }
}
