#[derive(Debug)]
pub enum ToolError {
    DeserializationError(String),
    NotEnoughRowsError(String),
    PathError(String),
    ReadError(String),
    TooManyRowsError(String),
    UnknownDrawer(String),
}
