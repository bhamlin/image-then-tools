use clap::{Parser, Subcommand};
use std::path::PathBuf;

#[derive(Debug, Parser)]
pub struct Args {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Search tree for CSVs and build a description of the keys present.
    #[command(name = "parse-types")]
    ExamineCsvs {
        /// Path to start walking for CSVs. Defaults to './data'.
        #[clap(default_value = "./data")]
        path: Option<PathBuf>,
    },
    /// Search tree for CSVs and import contents into database.
    #[command(name = "load-data")]
    Slurp {
        /// Path to start walking for CSVs. Defaults to './data'.
        #[clap(default_value = "./data")]
        path: Option<PathBuf>,
    },
}
