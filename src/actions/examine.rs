use crate::{
    error::ToolError,
    utils::{ensure_trailing_slash, map_csv, walk_path_for_csvs},
};
use image_then_common::model::{Document, HashMapGet};

use std::{
    collections::{HashMap, HashSet},
    path::PathBuf,
};

pub fn examine_csvs(path: Option<PathBuf>) -> Result<(), ToolError> {
    let path = path.unwrap();

    let mut ap_keys = HashSet::<String>::new();
    let mut cs_keys = HashSet::<String>::new();
    let mut collect_values = HashSet::<String>::new();

    let files = walk_path_for_csvs(&path)?;
    for file in files {
        let file_path = PathBuf::from(&file);

        let record = map_csv(&file_path)?;
        match record {
            Document::AccountingDocument(_) => (), // Ignore for now
            Document::ItDocument(_) => (),         // Ignore for now

            Document::AccountsPayableDocument(e) => {
                ap_keys.extend(e.keys().into_iter().map(|e| e.clone()));
            }
            Document::MemberServiceDocument(e) => {
                cs_keys.extend(e.keys().into_iter().map(|e| e.clone()));
            }

            _ => {
                return Err(ToolError::UnknownDrawer(format!(
                    "Unknown ({file}): {record:?}"
                )))
            }
        }
    }

    let mut ap_keys: Vec<_> = ap_keys.iter().collect::<Vec<_>>();
    let mut cs_keys: Vec<_> = cs_keys.iter().collect::<Vec<_>>();

    ap_keys.sort();
    cs_keys.sort();

    println!("Accounts Payable: {ap_keys:#?}");
    println!("Member Service: {cs_keys:#?}");

    Ok(())
}
