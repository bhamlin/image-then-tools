use crate::{
    error::ToolError,
    utils::{ensure_trailing_slash, map_csv, walk_path_for_csvs},
};
use image_then_common::model::Document;
use std::path::PathBuf;

pub fn do_slurp(path: Option<PathBuf>) -> Result<(), ToolError> {
    let path = path.unwrap();
    let path_str = ensure_trailing_slash(path.display().to_string());

    let files = walk_path_for_csvs(&path)?;

    for file in files {
        let file_path = PathBuf::from(&file);
        let relative_path = file.replace(&path_str, "/");
        println!("{relative_path}");
        let mut record = map_csv(&file_path)?;
        // record.insert("FilePath".to_string(), relative_path);
        match record {
            Document::MiscDocument(e) => println!("{:?}", e.get("DocumentID")),
            _ => (),
        }
    }

    Ok(())
}
